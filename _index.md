---
title: Global Best Practices
lastmod: 2020-12-01Z23:00:00Z
---

## Introduction

Following these global best practices enables {{% gls appcreator %}} to increase the
trustability of their {{% gls app %}} by {{% gls appuser %}}, by demonstrably reducing
the potential for risks and harms to {{% gls appuser %}} arising from the use of their
{{% gls app %}} by themselves or others. They cover {{% gl app %}} technology, operation
and governance.

These global best practices do this:

* by creating opportunities for {{% gls appuser %}}, third parties and the general
  public to find, raise and get resolved potential risks, harms or other issues for
  technology, operation and governance that {{% gls appcreator %}} did not find or
  resolve themselvesm, for whatever reasons;

* by reducing opportunities for {{% gls appcreator %}}, or individual insiders among
  {{% gl appcreator %}} organizations to inadvertently or intentionally deceive
  {{% gls appuser %}}.

As a result, {{% gls appuser %}} can have higher confidence in the trustworthiness of
{{% gls app %}} whose {{% gls appcreator %}} follow these best practices, over
{{% gls app %}} whose {{% gls appcreator %}} that do not.

This is a draft list, and expected to evolve over time.

## Potential risks or harms addressed

Specifically, we consider the following scenarios:

1. **App Creators commit inadvertent mistakes that can expose App Users to avoidable risks or harms ("MISTAKES")**

   * *Example:* because of a typo in the source code, an {{% gl app %}} is not performing an
     access control check on an {{% gl api %}}, as a result of which third parties can
     download certain {{% gl app %}} data without being authorized.

   * *Example:* an {{% gl appdeveloper %}} implements an algorithm that has a significant
     known security vulnerability which, however, is not known to the {{% gl appdeveloper %}}.

   * *Example:* communication between {{% gls appcreator %}}' engineering and marketing groups
     is insufficient and some information about the {{% gl app %}} published on the
     {{% gl app %}}'s official website is simply not true or not true any more as the
     {{% gl app %}}'s development has continued.

2. **App Creators use sub-standard architectures, data structures, algorithms, and the
   like, or apply sub-standard workmanship when creating, operating or governing the App
   ("SUBSTANDARD")**

   * *Example:* a {{% pageref "/features/contact-tracing/" %}} {{% gl app %}} requires
     {{% gls appuser %}} to provide their full name, address and social security number,
     although {{% pageref "/choicegroups/contact-tracing.md" "alternate approaches" %}} to
     implementing {{% pageref "/features/contact-tracing/" %}} based on
     {{% gl pseudonymity %}} are well-known and could have been used instead, creating
     unnecessary privacy risks.

   * *Example:* coding of an {{% gl app %}} is sloppy or uses "spaghetti code", which makes
     it hard for both reviewers and the {{% gls appdeveloper %}} themselves to be certain
     what the code actually does under all circumstances, thereby creating additional
     risks.

3. **App Creators deliberately deceive App Users ("DECEPTION")**

   * *Example:* an {{% gl app %}} contains undeclared spyware.

4. **Information published to App Users by App Creators is misleading ("MISLEADING")**

   * *Example:* the {{% gl app %}} portrays itself as (only) performing one type of
     functionality (e.g. contact tracing), but it also silently contains significant
     other functionality (e.g. it collects App Users' location history).

5. **While the App Creator organization(s) themselves do not intend to deceive App
   Users, some individuals in an App Creator organization perform an insider attack
   ("INSIDER")**

   * *Example:* a software developer adds "backdoor code" to the shipped
     {{% gl app %}} in order to spy on his ex-girlfriend, without the rest of the
     {{% gl appcreator %}} team knowing.

   * *Example:* a systems administrator creates additional backup tapes of
     confidential information and makes those available to third parties unbeknownst
     to the rest of the {{% gl appcreator %}} team, or to {{% gls appuser %}},
     for example because they have been bribed or blackmailed.

## Best practices: technology

1. The App implements **"best-of-class" architectures, data structures, algorithms and
   communication protocols**.

   For example, some {{% pageref "/choicegroups/architecture.md" architectures %}} are
   inherently more privacy-preserving (on-device data vs on-cloud data), as are some
   data structures (e.g. pseudonymous vs fully identified users).

   See the {{% pageref "/choicegroups/" "section on implementation choices" %}} and their
   pros and cons.

   Addresses:

   * SUBSTANDARD, e.g. unnecessary risks due to sloppy, insecure, or other substandard
     workmanship;

   * MISLEADING, e.g. the intentional use of weaker security than available.

1. **The full source code is available for review** by third parties, such as by being
   published to the general public on a website such as Github. The published code must
   include all relevant components (e.g. {{% gl smartphone-component %}} as well as
   {{% gl cloud-component %}}, needed libraries in the correct versions etc).

   For our purposes here, it is not necessary that the source code be licensed using
   a {{% gl foss %}} license; only that it is available for review, building,
   instrumenting and running for the purposes of evaluating its trustworthiness.

   Addresses:

   * MISTAKES, e.g. bugs in the code;

   * SUBSTANDARD, e.g. because developers fear for their reputation if they are
     associated with sloppy work;

   * DECEPTION, e.g. allows that the code does not contain hidden backdoors;

   * MISLEADING, e.g. the published code works as advertised;

1. **All dependencies follow the same best practices as the main App**. Those
   dependencies include static code dependencies (e.g. libraries), dynamic
   code dependencies (e.g. code loaded at run-time), on-line services
   (e.g. hosted analytics services), hosting services (e.g. IaaS platform) etc.

   This is necessary because otherwise, while the {{% gl app %}} itself may be
   trustworthy and low risk, {{% gls appuser %}} are at risk from included
   components by other developers.

1. **Complete build instructions are available** and can be easily followed by
   reviewers of the source code.

   Addresses:

   * MISTAKES, e.g. a submodule's source code was accidentally not published and
     thus could not be reviewed;

   * SUBSTANDARD, e.g. unreliable build process leading to inconclusive testing
     results;

1. The App can be **built with a {{% gl reproducible-build %}}**.

   This enables reviewers to verify that the published, downloadable version of the
   {{% gl app %}} is identical to the version whose source code has been published
   for review.

   Addresses:

   * DECEPTION, e.g. a submodule's source code was intentionally not published
     because it contained spyware that the {{% gls appcreator %}} intended to keep
     hidden;

   * INSIDER, e.g. because it would show that a rogue engineer modified/added/removed
     code during the release process.

   Note: unfortunately, the technical foundations of {{% gls reproducible-build %}}
   are still in their infancy, and even well-meaning {{% gls appcreator %}} may not be
   able to employ the technique (yet) in some circumstances, e.g. on iOS.

1. The App's **architecture, key data structures, key algorithms and communication protocols
   are publicly documented**.

   This aids reviewers in understanding the published code, in performing black-box
   testing, and makes it easier to find issues.

1. **Unit and system tests** are documented, available, and easily executable by reviewers.

   This enables reviewers to more easily understand the reviewed code. It also enables
   them to add tests to verify that additional scenarios (e.g. edge cases) also
   behave as expected.

1. A **security/risk analysis is publicly available** that documents known risks or
   potential vulnerabilities, their potential impact, and measures taken by the
   {{% gls appcreator %}} to mitigate them.

1. If the {{% gl app %}} includes any form of advertising, the full
   **privacy impact of advertising** in the {{% gl app %}} is clearly explained.

## Best practices: operations

1. All **published information is being kept up-to-date**, including documentation of
   technology, operations and governance.

   Out-of-date information prevents the detection of all kinds of issues for longer than
   necessary.

1. The App **development is performed in the open**. This includes:

   * a public issue tracker, which contains the known open issues, planned enhancements,
     discussion of such open issues and enhancements, as well as a history of closed
     issues and their disposition;

   * the ability for {{% gls appuser %}} and the general public to review and contribute
     to the public issue tracker;

   * public commit history of changes to the code base and tests;

   * ability for reviewers and the general public to access and test pre-release
     versions of the {{% gl App %}};

1. All **issues raised in the issue tracker are promptly and sufficiently addressed** in
   public by the {{% gls appcreator %}}, regardless of who raised them. This is best
   done within the issue tracker itself.

1. All known **attempted attacks are publicly documented**, whether they were successful
   or not. This includes all types of {{% gls attack %}}, including security breaches,
   {{% gl reidentification %}} attacks, {{% gl data-poisoning %}} etc.
   Impact of such attempted or successful attacks on {{% gls appuser %}} and others is
   clearly documented, as are the steps that were performed to recover.

1. **Independent review is encouraged**. This includes review of any or all aspects of
   technology, process and governance.

   This may go all the way to {{% gls appcreator %}} funding audits by independent,
   qualified third parties, or establishing an oversight board with members of the
   stakeholder community, including verifying that the financial results relating to
   the {{% gl app %}} are consistent with its declared business model. Depending on
   the members of the independent auditors and their trustworthiness, more or less
   detailed information should be released by them to the public.

1. The **day-to-day operations and their status are publicly documented**.

   This includes {{% gl app %}} development activities such as check-in's, and
   operational activities such as deletion of data that has reached the end of its
   retention period.

1. **Critical process steps require a second person**. For example, software commits
   may require sign-off from a second developer who is jointly responsible with the
   committer for the change.

1. **All contributors are vetted**, if they have a meaningful role in development,
   operations or governance of the {{% gl app %}}.

1. The **approach to protecting critical data is documented** and followed strictly.
   For security reasons, this need not be public.

   This includes items such as:

   * how backups are secured (e.g. encryption, storage)
   * how root keys are secured (e.g. background checks for authorized employees,
     physical security, use of threshold cryptography)
   * how monitoring in the field is performed

   and others, depending on the particulars of the {{% gl app %}}.

1. There is a **whistleblower process** by which an insider can anonymously, and
   effectively, raise issues with any aspect of technology, process or governance
   related to the {{% gl app %}}.

   Such a whistleblower process may be as simple as allowing insiders to log issues
   in the public bug tracker using an identity other than their work identity.

## Best practices: governance

1. The **full set of organizations is publicly documented** that are participating in any
   aspect of {{% gl app %}} creation, evolution, operations or governance, with their
   respective {{% gl appcreator %}} roles.

1. The {{% gl app %}}'s **financing and business model is publicly documented**.
   It is consistent with all other information available about the {{% gl app %}},
   and clearly explains which of the {{% gl appcreator %}} organizations provides
   or obtains resources to or from the {{% gl app %}}.

1. **Governance decisions are publicly documented**, e.g. as meeting minutes or in
   the issue tracking system.

1. Clear **remedies exist** that {{% gls appuser %}} can trigger should {{% gls appcreator %}}
   breach their trust. These remedies may exist under contract law or other forms of
   law.

1. **All independent reviews are available to the general public**. This includes
   positive and negative reviews; of course, {{% gls appcreator %}} are free to
   publish any rebuttal.
